# <center>《机器人操作系统入门》课程代码示例</center>

---

## 前言
欢迎来到中国大学MOOC---[**《机器人操作系统入门》**](https://www.icourse163.org/)课程，本ROS软件包是课程的代码示例，课程中使用的例子均出自本代码包。除了代码包，课程还提供[讲义](https://sychaichangkun.gitbooks.io/ros-tutorial-icourse163/content/)，欢迎各位朋友下载、学习和分享。

本示例包含了XBot机器人和中科院软件博物馆仿真、ROS通信示例程序、导航与SLAM功能演示，在每个软件包下都有相应的功能介绍。

![Gazebo仿真效果](./robot_sim_demo.gif)

如果你遇到任何问题，可以在Github上方的issues栏目中提问，我们课程团队会耐心回答。本示例将**长期维护**，**不断更新**，如果你认可我们的工作，请点击右上角的**star**按钮，您的鼓励是我们的动力。


---
## 示例介绍
本仓库为ROS入门教程的代码示例，包括以下ROS软件包:

| 软件包 | 内容 |
| :--- | :----: |
| **robot_sim_demo** | 机器人仿真程序，大部分示例会用到这个软件包 |
| **topic_demo** | topic通信，自定义msg，包括C++和python两个版本实现 |
| **service_demo** | service通信，自定义srv，分别以C++和python两种语言实现 |
| **action_demo** | action通信，自定义action，C++语言实现 |
| **param_demo** | param操作，分别以C++和python两种语言实现 |
| **msgs_demo** | 演示msg、srv、action文件的格式规范 |
| **tf_demo** | tf相关API操作演示，tf示例包括C++和python两个版本 |
| **name_demo** | 演示全局命名空间和局部命名空间下参数的提取 |
| **tf_follower** | 制作mybot机器人 实现mybot跟随xbot的功能 |
| **urdf_demo** |  创建机器人urdf模型，在RViz中显示  |
| **navigation_sim_demo** | 导航演示工具包，包括AMCL, Odometry Navigation等演示 |
| **slam_sim_demo** | 同步定位与建图演示，包括Gmapping, Karto, Hector等SLAM演示 |
| **orbslam2_demo** | ORB_SLAM2的演示 |
| **rtabmap_demo** | rtabmap的演示 |
| **cartographer_demo** | cartographer安装演示包|
| **dependlib** | ubuntu18所依赖的包(melodic不自带) |
| **ros_academy_for_beginners** | Metapacakge示例，依赖了本仓库所有的pacakge |


---

## 安装ROS(此版本用于Ubuntu 20.04)

```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
sudo apt update
sudo apt install ros-noetic-desktop-full python3-rosinstall python3-rosinstall-generator python3-wstool build-essential python3-rosdep -y
echo "source /opt/ros/noetic/setup.bash" >> ~/.bashrc
source ~/.bashrc
roscore #测试ros是否安装成功
```

## 设置

https://site.ip138.com/raw.Githubusercontent.com/

查找相关替换IP

```bash
sudo vim /etc/hosts
```

添加

```
185.199.108.133 raw.githubusercontent.com
```

```bash
sudo rosdep init
rosdep update
```

## 下载编译和运行

1. 克隆或下载ROS-Academy-for-Beginners教学包到工作空间的`/src`目录下，例如 `~/catkin_ws/src`
```sh
mkdir -p ~/catkin_ws/src
cd ~/catkin_ws/src
git clone https://gitee.com/yunxiangluo/ros_academy_code.git
```

2. 安装教学包所需的依赖
```sh
cd ~/catkin_ws
rosdep install --from-paths src --ignore-src --rosdistro=noetic -y
```

3. 编译并刷新环境
```sh
catkin_make
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
source ~/.bashrc
```

4. 启动仿真环境

```
roslaunch robot_sim_demo robot_spawn.launch
```

## 常见问题

1. 在使用qt5时可能会出现下列问题，说是libQt5Core.so.5找不到了

```bash
sudo apt-get install libqt5core5a
sudo strip --remove-section=.note.ABI-tag /lib/x86_64-linux-gnu/libQt5Core.so.5
```

2. rosdep update失败，手工编译工作空间成功后，启动仿真报controller错误

```bash
sudo apt-get install ros-melodic-effort-controllers ros-melodic-position-controllers -y
```

---
## Copyright

![Logo](./joint_logo.png)
